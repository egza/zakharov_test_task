const LoginPageData = require('../../data/LoginPageData');

class LoginPage {

    visit() {
        cy.visit(LoginPageData.url);
    }

    emailField() {
        return cy.get('#okta-signin-username');
    }

    passwordField() {
        return cy.get('#okta-signin-password');
    }

    fillEmail(email) {
        this.emailField().clear();
        this.emailField().type(email);

        return this;
    }

    fillPassword(password) {
        this.passwordField().clear();
        this.passwordField().type(password);

        return this;
    }

    submit() {
        const button = cy.get('#okta-signin-submit');
        button.click();
    }

    login(email, password) {
        this.fillEmail(email);
        this.fillPassword(password);
        this.submit();
    }

    loginError() {
        return cy.get(`.okta-form-infobox-error p`);
    }
}

module.exports = new LoginPage();