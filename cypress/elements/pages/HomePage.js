const HomePageData = require('../../data/HomePageData');

class HomePage {

    visit() {
        cy.visit(HomePageData.url);
    }

    goButton() {
        return cy.get('#Login');
    }
    
    clarizenGoClick() {
        this.goButton().click();
    }
}

module.exports = new HomePage();