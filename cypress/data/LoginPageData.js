'use strict';

module.exports = {
    url: 'https://go.clarizen.com/',
    signinFailedErrorMsg: 'Sign in failed!'
};
