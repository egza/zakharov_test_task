const LoginPage = require('../elements/pages/LoginPage');
const LoginPageData = require('../data/LoginPageData');
const HomePage = require('../elements/pages/HomePage');
const HomePageData = require('../data/HomePageData');
const faker = require('faker');
    
describe('Clarizen', () => {
    before(() => {
        HomePage.visit();
    });

    it('should open "Portal" page', () => {
        cy.url().should('eq', HomePageData.url);
        HomePage.goButton().should('be.visible');
    });


    it('should open "Go" on click "Clarizen-Go"', () => {
        HomePage.clarizenGoClick();
        LoginPage.emailField().should('be.visible');
        cy.url().should('eq', LoginPageData.url);
    });

    it('should show error message on invalid credentials', () => {
        LoginPage.login(faker.internet.email(), faker.internet.password(8));
        LoginPage.loginError()
            .should('be.visible')
            .should('have.text', LoginPageData.signinFailedErrorMsg);
    });
});
