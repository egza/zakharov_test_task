FROM cypress/base:10

COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm i

COPY cypress ./cypress
COPY cypress.json ./cypress.json

RUN ["npm","test"]