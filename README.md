Test task
===========
##### Here you can find e2e tests for Clarizen

## Task:
 1) Navigate to https://portal.clarizen.com/
 2) Select Clarizen Go and make sure the landing page was loaded
 3) Try to login using credentials and expect failure due to bad credentials


## To be able to run tests:
You should have Docker installed on your machine.

Execute:
```docker build -t cypress:1 .```
to build the image

## To run tests:
Execute:
```docker run -it cypress:1```

#### You can also run tests outside Docker:
1) You should have NodeJS installed on your machine
2) Run `npm i` to install all dependencies
3) Run `npm test` to run all specs or `npm test -s cypress/integration/${testname}.spec.js` to run specific test